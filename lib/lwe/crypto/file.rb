require "base64"
require "socket"
require "fileutils"
require "lwe/crypto/generator"

module LWE
  module Crypto
    # Tools for working with key files.
    class File
      # Default directory for storing keys
      DEFAULT_DIRECTORY = "~/.lwe"

      # Generate a private key file
      # @param [String] private_key the private key to store
      # @param [String] filename name of the key file
      # @param [String] path the path where the file should be stored
      def self.generate_private_key_file(private_key, filename="id_lwe", path=DEFAULT_DIRECTORY)
        initiate_directory(path) unless Dir.exist? path
        IO.write(::File.expand_path(::File.join(path, filename)), generate_private_key_contents(private_key))
      end

      # Generate a public key file
      # @param [Array] public_key the public key to store
      # @param [String] filename name of the key file
      # @param [String] path the path where the file should be stored
      def self.generate_public_key_file(public_key, bits=LWE::Crypto::DEFAULT_KEYSIZE, filename="id_lwe.pub", path=DEFAULT_DIRECTORY)
        initiate_directory(path) unless Dir.exist? path
        IO.write(::File.expand_path(::File.join(path, filename)), generate_public_key_contents(public_key, bits))
      end

      #private

      # @api private
      def self.generate_private_key_contents(private_key)
        sha = Digest::SHA1.new
        sha2 = Digest::SHA256.new
        sha2 << private_key.to_s
        payload = "#{private_key}/#{sha2.hexdigest}"
        sha << payload
        "-----BEGIN LWE PRIVATE KEY-----\n" +
        "Info: LWE-#{Tools.determine_bits(private_key)},#{sha.hexdigest.upcase}\n\n" +
        Base64.encode64(payload) +
        "-----END LWE PRIVATE KEY-----\n"
      end

      # @api private
      def self.generate_public_key_contents(public_key, bits=LWE::Crypto::DEFAULT_KEYSIZE)
        pubkey = Base64.strict_encode64 Zlib::Deflate.deflate(public_key.join("\n"), Zlib::BEST_COMPRESSION)
        sha = Digest::SHA1.new
        sha << pubkey

        user = sha.hexdigest.slice(0, 11)
        host = ""
        user = "#{ENV['USER']}@" if ENV.keys.member? 'USER'
        host = Socket.gethostname if ENV.keys.member? 'USER'

        "lwe-#{bits} BBBBC4O#{pubkey} #{user}#{host}\n"
      end

      # @api private
      def self.initiate_directory(path)
        FileUtils.mkdir_p ::File.expand_path(path)
      end

    end
  end
end