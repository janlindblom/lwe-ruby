module LWE
  module Crypto
    class Decrypt

      def self.decrypt(encrypted_message, private_key)
        byte_sequences = encrypted_message

        if encrypted_message.is_a? String
          # Check if it's a base64 string and if it's gzipped
          begin
            processed_message = Zlib.deflate(Base64.strict_decode64(encrypted_message))
          rescue Zlib::DataError
            # Not gzipped, assume it's just Base64-encoded then
            processed_message = Base64.strict_decode(encrypted_message)
          rescue ArgumentError
            # Not a base64 string,
            # assume it's on the format bit,bit,...,bit/bit,bit,...,bit
            processed_message = encrypted_message
          end
          
          byte_sequences = processed_message.split("/").map{ |byte|
            byte.split(",") }
        end

        byte_sequences.map{ |byte|
          byte.map{ |bit|
            (bit.to_f / private_key.to_f).to_s.split(".")[1].to_i % 2 } }
      end

      private

      def self.binarray_to_byte
        summators = [1, 2**1, 2**2, 2**3, 2**4, 2**5, 2**6, 2**7]
      end

    end
  end
end