module LWE
  module Crypto
    class Encrypt

      def self.encrypt(message, public_key)
        if message.is_a? String
          bit_sequences = message.bytes.map{ |byte| ("%08b" % byte).chars.map{ |y| y.to_i } }

          encrypted = []
          bit_sequences.each do |bit_sequence|
            encrypted_byte = []
            bit_sequence.each do |bit|
              sum_a = public_key.sample(public_key.length / 2).map{ |a| a }.reduce(0, :+)
              sum_a += 1 if bit == 1
              encrypted_byte << sum_a
            end
            encrypted << encrypted_byte
          end
          encrypted
        end
      end

      def self.encrypt_to_string(message, public_key)
        encrypt(message, public_key).map{ |byte| byte.join(",") }.join("/")
      end

    end
  end
end