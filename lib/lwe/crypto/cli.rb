require "thor"
require "lwe/crypto/generator"
require "lwe/crypto/file"

module LWE
  module Crypto
    # Command Line tool for generating keys
    class Cli < Thor
      class_option :bits, type: :numeric, aliases: :b
      default_command :generate
      
      map "-h" => :help
      map "--help" => :help
      map "-help" => :help

      desc "OPTIONS", "Generate public/private LWE key pair."
      # Generate LWE keypairs
      def generate
        bits = 512
        bits = options[:bits] if options[:bits]
        default_path = "~/.lwe"
        default_file = "id_lwe"

        say "Generating public/private LWE key pair."

        saved_file = ask "Enter file in which to save the key (#{default_path}/#{default_file}):", path: true
        saved_file = "#{default_path}/#{default_file}" if saved_file.length < 1
        file_path = ::File.expand_path(saved_file)
        fpparts = file_path.split(::File::SEPARATOR)
        fpparts.pop
        file_path = ::File.expand_path(fpparts.join(::File::SEPARATOR))
        base_name = ::File.basename(saved_file)
        # TODO: Add support for password protected keys
        # Enter passphrase (empty for no passphrase):
        # Enter same passphrase again:
        if ::File.exist?(::File.expand_path(saved_file))
          overwrite = ask "Overwrite existing file (y/N)?"
          do_overwrite = false
          do_overwrite = true if overwrite.match(/[Yy]/)
          return unless do_overwrite
        end

        private_key = LWE::Crypto::Generator.private_key bits
        public_key = LWE::Crypto::Generator.public_key(private_key, bits)
        LWE::Crypto::File.generate_private_key_file private_key, base_name, file_path
        LWE::Crypto::File.generate_public_key_file public_key, bits, "#{base_name}.pub", file_path

        say "Your identification has been saved in #{file_path}/#{base_name}."
        say "Your public key has been saved in #{file_path}/#{base_name}.pub."
      end

      desc "-h [COMMMAND]", "Describe available commands or one specific command"
      # Show help for the CLI
      def help
        puts <<-HELPTEXT
usage: lwe-keygen [options]
Options:
  -h command  Describe available commands or one specific command
  -b bits     Number of bits in the key to create.
HELPTEXT
      end

    end
  end
end