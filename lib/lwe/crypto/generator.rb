require "simple-random"
require "socket"
require "base64"
require "prime"

module LWE
  module Crypto
    # Tools for generating LWE Crypto keys
    class Generator

      # Generate an LWE keypair.
      # @param [Integer] bits the number of bits of the private key
      # @return [Hash] a hash with the keys stored as ":private" and ":public"
      def self.generate_keypair(bits=LWE::Crypto::DEFAULT_KEYSIZE)
        privkey = private_key bits
        pubkey = public_key privkey

        {
          private: privkey,
          public: pubkey
        }
      end

      # Generate a private key
      # @param [Integer] bits the number of bits
      # @return [String] the private key as a hex string
      def self.private_key(bits=LWE::Crypto::DEFAULT_KEYSIZE)
        odd_random_integer_in_range 0, (2**bits-1)
      end

      # Generate a public key
      # 
      # Adheres to the rules outlined in the specification with the following
      # adaptations:
      # 
      # q ≥ 2, a prime number between n² and 2n².
      # <strike>m=(1+ϵ)(n+1) log(q) for an arbitrary constant ϵ</strike>
      # 
      # Where n = number of bits and ϵ a random number between 1 and n.
      # 
      # @param [String] private_key the private key to create a public key for
      # @return [Array] an array with the public key
      def self.public_key(private_key, bits=LWE::Crypto::DEFAULT_KEYSIZE, dimensions=16)
        raise ArgumentError.new "Unsupported number of bits: #{bits}. Only supported number of bits are: #{approved_key_sizes}." unless approved_key_sizes.member? bits

        pubkey = []
        last_key = private_key
        sizing = public_key_sizing bits
        # currently not using q
        #q = calculate_q_between(dimensions ** 2, 2 * dimensions ** 2)
        eps = random_integer_in_range(2, dimensions)
        m = dimensions # or we should do ((1+eps)*(n+1)*Math.log(q)).to_i

        (1..m).each do |index|
          p1 = last_key * random_integer_in_range(1, 2**sizing)
          p2 = even_random_integer_in_range(1, bits)
          last_key = p1 + p2
          pubkey << last_key
        end

        pubkey
      end

      #private

      # @api private
      def self.random_integer_in_range(lower, upper)
        sr = SimpleRandom.new
        sr.set_seed LWE::Crypto::Seeder.generate_seed
        #bits = determine_bits(upper)
        #bytes = bits / 8
        #num = (1..bytes).map{ |byte| ("%002x" % sr.uniform(0, 0xff).to_i) }.join
        #num = ("%x" % sr.uniform(lower, upper).to_i)
        #num.hex
        sr.uniform(lower, upper).to_i
      end

      def self.parallelize_find_integer_in_range(lower, upper)
        n = number_of_intervals lower, upper
        intervals = []
        
      end

      def self.fixed_ranges(intervals)
        ranges = {
          1 => [0..2**8-1],
          2 => [2**8..2**16-1],
          3 => [2**16..2**32-1],
          4 => [2**32..2**64-1],
          5 => [2**64..2**128-1],
          6 => [2**128..2**256-1],
          7 => [2**256..2**512-1],
          8 => [2**512..2**1024-1],
          9 => [2**1024..2**2048-1]
        }
        (1..intervals).map{ |i| ranges[i] }.flatten
      end

      def self.number_of_intervals(lower, upper)
        range = upper - lower
        intervals = case range
          when       0..2**8-1    then 1
          when    2**8..2**16-1   then 2
          when   2**16..2**32-1   then 3
          when   2**32..2**64-1   then 4
          when   2**64..2**128-1  then 5
          when  2**128..2**256-1  then 6
          when  2**256..2**512-1  then 7
          when  2**512..2**1024-1 then 8
          when 2**1024..2**2048-1 then 9
        end
        intervals
      end

      # @api private
      def self.random_integer_in_range_with_sign(lower,upper,sign)
        constraint_fulfilled = false
        num = nil
        num = random_integer_in_range(lower, upper)
        case sign
          when :odd
            num += 1 unless num.odd?
          when :even
            num += 1 unless num.even?
        end
        num
      end

      # @api private
      def self.even_random_integer_in_range(lower, upper)
        random_integer_in_range_with_sign(lower, upper, :even)
      end

      # @api private
      def self.odd_random_integer_in_range(lower, upper)
        random_integer_in_range_with_sign(lower, upper, :odd)
      end

      # @api private
      def self.calculate_q_between(lower, upper)
        sr = SimpleRandom.new
        sr.set_seed LWE::Crypto::Seeder.generate_seed
        num = 0

        while !(Prime.prime? num and num != 2)
          num = sr.uniform(lower, upper).ceil
        end

        num
      end

      # @api private
      def self.public_key_sizing(bits)
        case bits
          when 2048 then 64
          when 1024 then 32
          when 512 then 16
          when 256 then 8
          when 128 then 4
          else 2
        end
      end

      # @api private
      def self.approved_key_sizes
        [16,32,64,128,256,512,1024,2048]
      end
    end
  end
end