require "digest/sha2"
require "socket"

module LWE
  module Crypto
    # Tools for generating seeds
    class Seeder

      # Generate a seed based on the following information:
      # 
      # * The time right now
      # * The user name of the current user (if available)
      # * The host name of the computer (if available)
      # * All IP addresses of the computer (if available)
      # @return [Integer] an integer with random seed
      def self.generate_seed
        now = Time.now.to_f
        "generating seed at #{now}"
        user = ENV['USER']
        hostname = Socket.gethostname
        ips = Socket.getifaddrs.select{ |x|
          x.addr.ip? }.select{ |x|
            !x.addr.ipv4_loopback? }.select{ |x|
              !x.addr.ipv6_loopback? }.map{ |x|
                x.addr.ip_address }.join("/")
        str = "#{now}: #{user} @ #{hostname}/#{ips}"
        sha2 = Digest::SHA256.new
        sha2 << str
        now2 = Time.now
        "seed generated at #{now2}"
        (sha2.hexdigest.hex / (now2.to_f ** 7)).to_i
      end
    end
  end
end