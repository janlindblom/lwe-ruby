require "base64"

module LWE
  module Crypto
    # Tools for parsing LWE Crypto key data
    class Parser
      # The pattern of a public key file
      PUBLIC_PATTERN = /lwe-(\d+)\W*BBBBC4O([A-z0-9\+\/\-\_\=]*)\W*([A-z0-9\.\@]*)$/

      # Parse a public key that follows the defined format described by
      # the constant "PATTERN".
      # @param [String] public_key the packed public key to parse
      # @return [Array] an array with the public key stored in this String
      def self.parse_public_key(public_key)
        kfparts = public_key.match PUBLIC_PATTERN
        throw ArgumentError.new("Malformed public key.") unless kfparts

        pkstr = kfparts[2] # base64 encoded, newline-separated, list w/ pubkey
        Zlib.inflate(Base64.strict_decode64(pkstr)).split("\n")
      end

      # Parse a private key that follows the defined format.
      # @param [String] private_key the packed private key to parse
      # @return [Array] a string with the private key
      def self.parse_private_key(private_key)
        sha = Digest::SHA1.new
        kfparts = private_key.split "\n"
        info = kfparts[1]
        keystr = kfparts[3..kfparts.length-2].join
        key = Base64.strict_decode64 keystr
        sha << key
        unless info.split(",").member? sha.hexdigest.upcase
          throw StandardError("Checksum doesn't check out, corrupted file.")
        end
        key
      end

    end
  end
end