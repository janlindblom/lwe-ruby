module LWE
  module Crypto
    class Tools

      # Determine the number of bits in which the given number fits.
      # @api private
      # @param [Integer] number the number to fit.
      # @return [Integer] an appropriate number of bits.
      def self.determine_bits(number)
        case number
          when       0..2**8-1    then 8
          when    2**8..2**16-2   then 16
          when   2**16..2**32-1   then 32
          when   2**32..2**64-1   then 64
          when   2**64..2**128-1  then 128
          when  2**128..2**256-1  then 256
          when  2**256..2**512-1  then 512
          when  2**512..2**1024-1 then 1024
          when 2**1024..2**2048-1 then 2048
        end
      end
    end
  end
end