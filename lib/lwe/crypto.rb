require "lwe/crypto/tools"
require "lwe/crypto/seeder"
require "lwe/crypto/generator"
require "lwe/crypto/parser"
require "lwe/crypto/file"
require "lwe/crypto/encrypt"
require "lwe/crypto/decrypt"

module LWE
  # Crypto module of the lwe-ruby toolkit
  module Crypto
    # Default number of bits in keys
    DEFAULT_KEYSIZE = 32
  end
end