module LWE
  module Fields
    class RealField

      def initialize(prec=53)
        @prec = prec
      end

      def member(n)
        n.round (@prec / 2).to_i
      end

    end
  end
end