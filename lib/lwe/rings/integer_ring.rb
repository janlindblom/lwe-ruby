module LWE
  module Rings
    # The Integer Ring.
    class IntegerRing

      # Return the given number on the ring of integers.
      # 
      # Alphanumeric strings can be used;
      # letters a to z represent numbers 10 to 36. Letter case does not matter.
      # 
      # @param [Numeric,String] n the number to fetch from the ring
      # @return [Integer] the number n on the integer ring
      def member(n)
        IntegerRing.member(n)
      end

      # Class method version of `#member`. Requires no object creation.
      # 
      # @param [Numeric,String] n the number to fetch from the ring
      # @return [Integer] the number n on the integer ring
      def self.member(n)
        # Deal with strings
        if n.is_a? String
          # From http://doc.sagemath.org/html/en/reference/rings_standard/sage/rings/integer_ring.html
          # Alphanumeric strings can be used for bases 2..36;
          # letters a to z represent numbers 10 to 36.
          # Letter case does not matter.
          return n.downcase.to_i(36) if n.downcase.match(/^[a-z]+$/)
          # Floats
          return n.to_f.round if n.match(/^[0-9]+\.[0-9]+$/)
          # Integers
          return n.to_i if n.match(/^[0-9]+$/)
        end
        n.round
      end

      # Polynomial sum function generator
      def self.polynomial_sum
        lambda do |coeff|
          lambda do |x|
            sum = 0
            coeff.reverse.each_with_index do |n, i|
              sum += n * x**i
            end
            sum
          end
        end
      end

      # TODO: Make this?
      def self.polynomial_sum_function(coeff)
        lambda do |x|
          sum = 0
          coeff.reverse.each_with_index do |n, i|
            sum += n * x**i
          end
          sum
        end
#        lambda do |factors, x|
#          sum = 0
#          factors.reverse.each_with_index do |n, i|
#            sum += n * x**i
#          end
#          sum
#        end
      end

    end

    # The ZZ ring, a wrapper around the IntegerRing class.
    class ZZ < IntegerRing

      # Wrapper around #integer_ring
      # 
      # @param [Numeric, String] n the number to get in ZZ
      # @return [Integer] the number n in ZZ
      def member(n)
        ZZ.member(n)
      end

      # Class method version of `#zz`. Requires no object creation.
      # 
      # @param [Numeric, String] n the number to get in ZZ
      # @return [Integer] the number n in ZZ
      def self.member(n)
        IntegerRing.member(n)
      end

    end

  end
end