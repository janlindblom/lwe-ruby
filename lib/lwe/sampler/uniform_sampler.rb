require "simple-random"
require "lwe/rings/integer_ring"

module LWE
  module Sampler
    # Uniform sampling in a range of integers.
    class UniformSampler
      attr_accessor :lower_bound, :upper_bound

      # Construct a uniform sampler with bounds `lower_bound` and
      # `upper_bound` (both endpoints inclusive).
      # 
      # Example:
      # sampler = LWE::Sampler::UniformSampler.new(-2, 2)
      # sampler.sample
      # -2
      # @param [Integer] lower_bound the lower bound
      # @param [Integer] upper_bound the upper bound
      def initialize(lower_bound, upper_bound)
        raise ArgumentError.new "lower bound must be <= than upper bound." if lower_bound > upper_bound

        self.lower_bound = LWE::Rings::IntegerRing.member lower_bound
        self.upper_bound = LWE::Rings::IntegerRing.member upper_bound
      end

      # Return a new sample.
      def sample
        rnd = SimpleRandom.new
        rnd.set_seed
        random = rnd.uniform(self.lower_bound - 1, self.upper_bound + 1)
        LWE::Rings::IntegerRing.member random
      end
    end

    # Uniform sampler for polynomials.
    class UniformPolynomialSampler
      attr_accessor :n, :lower_bound, :upper_bound, :p, :d

      # Construct a sampler for univariate polynomials of degree ``n-1`` where
      # coefficients are drawn uniformly at random between ``lower_bound`` and
      # ``upper_bound`` (both endpoints inclusive).
      # 
      # @param [Integer] n polynomial degree
      # @param [Integer] lower_bound the lower bound
      # @param [Integer] upper_bound the upper bound
      def initialize(n, lower_bound, upper_bound)
        self.n = LWE::Rings::IntegerRing.member n
        if lower_bound > upper_bound
            raise ArgumentError.new "lower bound must be <= than upper bound."
        self.lower_bound = LWE::Rings::IntegerRing.member(lower_bound)
        self.upper_bound = LWE::Rings::IntegerRing.member(upper_bound)
        self.p = LWE::Rings::IntegerRing.polynomial_sum
        self.d = UniformSampler.new(self.lower_bound, self.upper_bound)
      end

      # Return a new sample.
      def sample
        coeff = []
        (0..(self.n)).each do |x|
          coeff << self.d.sample
        end
        f = self.p[coeff]
        f
      end
    end
  end
end