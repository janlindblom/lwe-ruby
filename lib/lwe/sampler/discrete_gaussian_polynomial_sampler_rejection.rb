require "lwe/rings/integer_ring"
require "lwe/sampler/discrete_gaussian_sampler"

module LWE
  module Sampler
    class DiscreteGaussianPolynomialSamplerRejection
      include Rings
      include LWE::Sampler::Math

      attr_accessor :stddev, :precision, :tailcut, :d, :n, :p

      def initialize(n, stddev, precision=53, tailcut=4, d=nil)
        self.stddev = stddev.to_f
        self.precision = precision
        self.tailcut = tailcut
        self.d = d.nil? ? DiscreteGaussianSampler.new(stddev, precision, tailcut) : d
        self.n = LWE::Rings::IntegerRing.member n
        self.p = LWE::Rings::IntegerRing.polynomial_sum
      end

      def sample
        coeff = []
        (0..(self.n)).each do |x|
          coeff << self.d.sample
        end
        f = self.p[coeff]
        f
      end
    end
  end
end