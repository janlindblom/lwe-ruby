module LWE
  module Sampler
    module Math

      # @api private
      def log(n)
        ::Math.log n
      end

      # @api private
      def log2(n)
        ::Math.log2 n
      end

      # @api private
      def exp(n)
        ::Math.exp n
      end

      # @api private
      def round(n, prec=nil)
        return n.round(prec) unless prec.nil?
        n.round
      end
    end
  end
end