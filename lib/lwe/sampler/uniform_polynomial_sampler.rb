require "lwe/rings/integer_ring"
require "lwe/sampler/uniform_sampler"

module LWE
  module Sampler
    # Uniform sampler for polynomials.
    class UniformPolynomialSampler
      attr_accessor :n, :lower_bound, :upper_bound, :p, :d

      # Construct a sampler for univariate polynomials of degree ``n-1`` where
      # coefficients are drawn uniformly at random between ``lower_bound`` and
      # ``upper_bound`` (both endpoints inclusive).
      # 
      # @param [Integer] n polynomial degree
      # @param [Integer] lower_bound the lower bound
      # @param [Integer] upper_bound the upper bound
      def initialize(n, lower_bound, upper_bound)
        self.n = LWE::Rings::IntegerRing.member n
        raise ArgumentError.new "lower bound must be <= than upper bound." if lower_bound > upper_bound

        self.lower_bound = LWE::Rings::IntegerRing.member lower_bound
        self.upper_bound = LWE::Rings::IntegerRing.member upper_bound
        self.p = LWE::Rings::IntegerRing.polynomial_sum
        self.d = LWE::Sampler::UniformSampler.new self.lower_bound, self.upper_bound
      end

      # Return a new sample.
      def sample
        coeff = []
        (0..(self.n)).each do |x|
          coeff << self.d.sample
        end
        f = self.p[coeff]
        f
      end
    end
  end
end