require "simple-random"
require "lwe/sampler/math"
require "lwe/fields"
require "lwe/rings"

module LWE
  module Sampler
    class DiscreteGaussianSamplerRejection
      include Rings
      include LWE::Sampler::Math

      attr_accessor :stddev, :precision, :tailcut

      attr_reader :rho, :max_precs, :upper_bound

      def initialize(stddev, precision=53, tailcut=4)
        self.stddev = stddev.to_f
        self.precision = precision
        self.tailcut = tailcut
        @inner_rho = nil
      end

      def sample
        x = 0
        y = 0
        z = 0
        while y >= z
          x = randint 0, self.upper_bound-1
          y = randint 0, self.max_precs-1
          z = rho[x]
        end
        rnd = SimpleRandom.new
        rnd.set_seed
        zero_or_one = LWE::Rings::IntegerRing.member rnd.uniform(0, 1)
        (2 * zero_or_one - 1) * x
      end

      def max_precs
        2**self.precision
      end

      def upper_bound
        LWE::Rings::ZZ.member tailcut * stddev
      end

      def rho(x=nil)
        if @inner_rho.nil? || @inner_rho.empty?
          r = LWE::Fields::RealField.new self.precision
          @inner_rho = []
          (0..self.upper_bound).each do |x|
            @inner_rho << round(self.max_precs * exp((-(r.member(x) / r.member(self.stddev))**2)/r.member(2)))
          end
          @inner_rho[0] = @inner_rho[0] / 2
        end
        return @inner_rho if x.nil?
        @inner_rho[x]
      end

      private

      def randint(lower, upper)
        rnd = SimpleRandom.new
        rnd.set_seed
        LWE::Rings::IntegerRing.member rnd.uniform(lower, upper)
      end

    end
  end
end