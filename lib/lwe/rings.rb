require "lwe/fields"
require "lwe/rings/integer_ring"

module LWE
  module Rings

    def rr(n)
      real_ring(n)
    end

    def real_ring(n, prec=53)
      r = LWE::Fields::RealField.new prec
      r.member(n)
    end




  end
end