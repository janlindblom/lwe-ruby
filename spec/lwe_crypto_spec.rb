require 'spec_helper'

describe LWE::Crypto::Generator do

  context "without a private key" do
    it "generates a private key" do
      private_key = LWE::Crypto::Generator.private_key
      expect(private_key).to_not be_nil
      expect(private_key).to be_a(String)
    end
  end

  context "with a private key" do
    before(:context) do
      @private_key = LWE::Crypto::Generator.private_key
    end
    it "generates a public key" do
      @public_key = LWE::Crypto::Generator.public_key @private_key
      expect(@public_key).to_not be_nil
      expect(@public_key).to be_a(Array)
      @public_key.each do |part|
        expect(part).to_not be_nil
        expect(part).to be_a(String)
        expect(part.hex).to be_a(Integer)
      end
    end
  end

  context "with both private and public key" do
    expect(true).to_not be_false
  end
end
