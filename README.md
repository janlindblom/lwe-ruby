# lwe-ruby

This is "lwe-ruby", a Ruby implementation of the LWE crypto scheme.

Don't expect too much performance.

**NOTE:** This is a work in project. Also, it might not strictly adhere to the rules defined by Regev in the initial proposal. This because of the complexity of Regev's design plus "I'll get there".

The basic point is: this works. Fully usable to generate keys, encrypt data and decrypt data. Key sizes have been selected to optimise for working with *bytes*, this means that the public key is generated with `m=16` instead of the outlined "m=(1+ϵ)(n+1) log(q) for an arbitrary constant ϵ".

## Background

What is LWE? You can read up on it here:

* https://en.wikipedia.org/wiki/Learning_with_errors
* http://www.iacr.org/conferences/eurocrypt2010/talks/slides-ideal-lwe.pdf

In summary, LWE is a crypto scheme working with large numbers. This package aims to make a simple solution available as to work with encryption and decryption using the LWE scheme.

LWE is also believed to be hard even for quantum computers.

See the blog post at http://www.janlindblom.se/blog/research/crypto/2015/09/21/learning-with-errors.html for more background and some discussion on how and why this was built.

## Attributions

This library was inspired by and in part ported from `lwe-generator` by Martin Albrecht et. al. See https://bitbucket.org/malb/lwe-generator/wiki/Home for more information.

## Adaptions

In order to make LWE easy to use, "lwe-ruby" can create files in your home directory that stores private and public keys much like OpenSSL/OpenSSH does.

Also, these files try to adhere to the same format as OpenSSL/OpenSSH key files for the sake of keeping things neat.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lwe-ruby'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lwe-ruby

## Usage

Use it in your Ruby project like so:

```ruby
require "lwe/crypto"

bits = 256
private_key = LWE::Crypto::Generator.private_key(bits)
```

Or use the command like tool, similar to `ssh-keygen`:

```bash
$ lwe-keygen -b 512
Generating public/private LWE key pair.
Enter file in which to save the key (~/.lwe/id_lwe): 
Overwrite existing file (y/N)? y
Your identification has been saved in /Users/jan/.lwe/id_lwe.
Your public key has been saved in /Users/jan/.lwe/id_lwe.pub.
```

## Known Issues

* Right now, the bit-size calculation is off at random times in the public key generator. This is probably just a minor thing to fix but priority is low.

## Contributing

1. Fork it ( https://bitbucket.org/janlindblom/lwe-ruby/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
